import { Character, CharacterDto } from "../core/Character";
import { Position } from "../core/Position";
import { CharacterFactory } from "../factory/CharacterFactory";
import { EnemyIA } from "../state/EnemyIA";



export class Game {
    private static instance: Game;
    private characters: Character[];
    private enemies:Character[];
    private ia:EnemyIA[];
    private selected: Character;


    private constructor() {
        
        this.characters = [];
        this.enemies = [];
        this.ia = [];
    }

    static getInstance(): Game {
        if (!Game.instance) {
            Game.instance = new Game();
        }
        return Game.instance;
    }

    select(position:Position) {
        let tile = this.characters.find(char => (
            char.getPosition().x === position.x 
            && char.getPosition().y === position.y)
        );
        if(!tile) {
            console.log('Selection failed : no character at this position');
        }
        this.selected = tile;
        console.log('Selected character '+this.selected.getName());
    }

    move(direction:string) {
        if(!this.selected) {
            console.log('Move failed : no selected character');
        }
        this.selected.move(direction);
        console.log(this.selected.getName()+ ' moved to x:'+this.selected.getPosition().x + ' y:'+this.selected.getPosition().y);
    }

    listCharacter(selectable:boolean = true): CharacterDto[] {
        if(selectable) {
            return this.characters.map(char => char.getDisplay());

        } else {
            return this.characters.concat(this.enemies).map(char => char.getDisplay());
        }
    }

    buildUnit(type:string) {
        let chara = CharacterFactory.createCharacter(type);
        this.characters.push(chara);
    }

    addEnemy() {
        let enemy = CharacterFactory
        .createCharacter(['warrior', 'archer', 'mage'][Math.floor(Math.random() * 3)]);
        this.enemies.push(enemy);
        this.ia.push(new EnemyIA(enemy));
    }

    enemyTurn() {
        for(let enemy of this.ia) {
            enemy.action();
        }
    }
    

}