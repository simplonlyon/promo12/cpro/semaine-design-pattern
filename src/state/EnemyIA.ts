import { Character } from "../core/Character";
import { IdleState } from "./IdleState";
import { IEnemyState } from "./IEnemyState";

export class EnemyIA {

    private state:IEnemyState;

    constructor(enemy:Character) {
        this.state = new IdleState(this, enemy);
    }

    action():void {
        this.state.action();
    }

    changeState(newState:IEnemyState) {
        this.state = newState;
    }

}