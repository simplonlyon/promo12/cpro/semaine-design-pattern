import { Character, CharacterDto } from "../core/Character";
import { AttackState } from "./AttackState";
import { EnemyIA } from "./EnemyIA";
import { IEnemyState } from "./IEnemyState";


export class ChaseState implements IEnemyState {
    constructor(private context:EnemyIA, private enemy:Character, private target:CharacterDto){}
    
    
    action(): void {
        let enemyPos = this.enemy.getPosition();
        if(enemyPos.x < this.target.position.x) {
          this.enemy.move('right');  
        }else if(enemyPos.x > this.target.position.x) {
          this.enemy.move('left');  
            
        }else if(enemyPos.y < this.target.position.y) {
            this.enemy.move('down');
        } else {
            this.enemy.move('up');
        }
        console.log('Enemy ' + this.enemy.getName() + ' is moving to x:' + this.enemy.getPosition().x + ' y: ' + this.enemy.getPosition().y);

        if(this.enemy.canAttack(new Character('stub',this.target.position))){
            console.log('Enemy ' + this.enemy.getName() + ' is preparing to attack '+this.target.name);
            this.context.changeState(new AttackState(this.context, this.enemy, this.target));
        }
    }

}