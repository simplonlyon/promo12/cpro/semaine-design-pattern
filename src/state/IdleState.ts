import { Character } from "../core/Character";
import { samePosition } from "../core/Position";
import { Game } from "../singleton/Game";
import { ChaseState } from "./ChaseState";
import { EnemyIA } from "./EnemyIA";
import { IEnemyState } from "./IEnemyState";


export class IdleState implements IEnemyState {

    constructor(private context: EnemyIA, private enemy: Character) { }

    action(): void {
        let randomDirection = ['up', 'down', 'left', 'right'][Math.floor(Math.random() * 4)];
        this.enemy.move(randomDirection);
        console.log('Enemy ' + this.enemy.getName() + ' is moving to x:' + this.enemy.getPosition().x + ' y: ' + this.enemy.getPosition().y);
        let characters = Game.getInstance().listCharacter();

        let target = characters.find(char => Math.sqrt(Math.pow(char.position.x - this.enemy.getPosition().x, 2) + Math.pow(char.position.y - this.enemy.getPosition().y, 2)) < 5)
        if (target) {
            console.log('Enemy ' + this.enemy.getName() + ' found a target !');

            this.context.changeState(new ChaseState(this.context,this.enemy,target));
        }
    }

}