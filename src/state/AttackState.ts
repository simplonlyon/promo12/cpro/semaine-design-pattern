import { Character, CharacterDto } from "../core/Character";
import { EnemyIA } from "./EnemyIA";
import { IEnemyState } from "./IEnemyState";


export class AttackState implements IEnemyState {
    constructor(private context:EnemyIA, private enemy:Character, private target:CharacterDto){}
    
    
    action(): void {
        console.log(this.enemy.getName()+' is attacking '+this.target.name)
    }
}