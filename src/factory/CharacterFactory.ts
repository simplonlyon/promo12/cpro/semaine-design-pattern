import { Character } from "../core/Character";
import { Position } from "../core/Position";
import { Game } from "../singleton/Game";
import { Bow } from "../strategy/Bow";
import { IWeapon } from "../strategy/IWeapon";
import { SpellBook } from "../strategy/SpellBook";
import { Sword } from "../strategy/Sword";


export class CharacterFactory {

    static createCharacter(type:string):Character{
        let name = "Chara"+Math.round(Math.random()*10);


        let chara = new Character(name, this.calculatePosition());
        chara.equip(CharacterFactory.createWeapon(type));


        return chara;
    }

    static createWeapon(type:string):IWeapon {
        switch(type) {
            case "warrior":
                return new Sword();
            case "archer":
                return new Bow();
            case "mage":
                return new SpellBook();
        }
    }

    private static calculatePosition(position = {x:0, y:0}):Position {

        let existingPositions = 
        Game.getInstance().listCharacter(false).map(chara => chara.position);

        if(existingPositions.find(pos => pos.x === position.x && pos.y === position.y)) {
            position = this.calculatePosition({x:Math.floor(Math.random() * 10), y:Math.floor(Math.random() * 10)});
        }

        return position;
    }
}