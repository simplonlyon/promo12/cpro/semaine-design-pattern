
export interface Position {
    x: number;
    y: number;
}


export function samePosition(pos1:Position, pos2:Position):boolean {
    return pos1.x === pos2.x && pos1.y === pos2.y;
}