import { IWeapon } from "../strategy/IWeapon";
import { NoWeapon } from "../strategy/NoWeapon";
import { Position } from "./Position";

export interface CharacterDto {
    name:string;
    position: Position;
}

export class Character {
    private weapon:IWeapon;

    constructor(private name:string,
                private position:Position,
                private hp:number = 100,
                private mp:number = 100,
                private stamina:number = 100){
        this.equip(new NoWeapon());
    }

    canAttack(target:Character):boolean {
        return this.weapon.canAttack(target);
    }

    equip(weapon:IWeapon) {
        this.weapon = weapon;
        this.weapon.setCharacter(this);
    }

    attack(target:Character) {
        if(this.canAttack(target)) {
            let damage = this.weapon.attack(target);
            console.log(this.name+" attacked "+target.name+" for "+damage+" damage");
        } else {
            console.log("can't attack target "+target.name);
        }
    }

    getPosition():Position {
        return this.position;
    }

    changeHp(amount:number) {
        this.hp += amount;
    }
    changeMp(amount:number) {
        this.mp += amount;
    }
    changeStamina(amount:number) {
        this.stamina += amount;
    }
     
    move(direction:string) {
        switch(direction) {
            case 'up': 
                this.position.y--;
                break;
            case 'down':
                this.position.y++;
                break;
            case 'left':
                this.position.x--;
                break;
            case 'right':
                this.position.x++;
                break;
        }
        if(this.position.x < 0) {
            this.position.x = 0;
        }

        if(this.position.y < 0) {
            this.position.y = 0;
        }
    }

    getName():string {
        return this.name;
    }

    getDisplay(): CharacterDto {
        return {
            name: this.name,
            position: this.position
        }
    }
    
}