import { Character } from "./core/Character";
// import { prompt } from "inquirer";
// import { ICliCommand } from "./command/ICliCommand";
// import { MainMenu } from "./command/MainMenu";
// import { Game } from "./singleton/Game";

import { Game } from "./singleton/Game";
import { EnemyIA } from "./state/EnemyIA";
import { Bow } from "./strategy/Bow";



// async function cli(commandList?:ICliCommand[]) {
//     if(!commandList) {
//         commandList = new MainMenu().execute();
//     }
//     let answers = await prompt([
//         {
//             type: "list",
//             name: "menu",
//             message: "Choose an action",
//             choices: commandList.map(command => ({
//                 name: command.getName(),
//                 value: () => command.execute()
//             }))
//         }
//     ]);
    
//     cli(answers.menu());
// }

// async function selectMenu() {
//     const game = Game.getInstance();
//     let answers = await prompt([
//         {
//             type: "list",
//             name: "select",
//             message: "Select a character",
//             choices: game 
//                     .listCharacter()
//                     .map(char => ({
//                 name: char.name,
//                 value: char.position
//             }))
//         }
//     ]);

//     game.select(answers.select);
// }

// cli();


let game = Game.getInstance();

game.buildUnit('warrior');

game.addEnemy();

game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
game.enemyTurn();
