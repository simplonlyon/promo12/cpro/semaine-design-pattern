import { Character } from "../core/Character";
import { Enchant } from "./Enchant";

export class MightyEnchant extends Enchant {

    attack(target:Character):number {
        target.changeHp(-5);
        return this.weapon.attack(target) + 5;
    }
    getName():string {
        return "Mighty "+this.weapon.getName();
    }
}