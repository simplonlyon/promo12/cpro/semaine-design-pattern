import { Character } from "../core/Character";
import { Enchant } from "./Enchant";

export class HpDrainEnchant extends Enchant {

    attack(target:Character): number {
        let damage = this.weapon.attack(target);
        this.weapon.getCharacter().changeHp(damage);
        return damage;
    }

    getName():string {
        return this.weapon.getName() + ' of the vampire';
    }
}