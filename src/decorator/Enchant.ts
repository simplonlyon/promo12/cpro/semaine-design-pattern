import { Character } from "../core/Character";
import { IWeapon } from "../strategy/IWeapon";

export abstract class Enchant implements IWeapon {

    constructor(protected weapon:IWeapon){}

    canAttack(target: Character): boolean {
        return this.weapon.canAttack(target);
    }
    attack(target: Character): number {
        return this.weapon.attack(target);
    }
    getName(): string {
        return this.weapon.getName();
    }

    getCharacter() {
        return this.weapon.getCharacter();
    }
    setCharacter(character: Character): void {
        this.weapon.setCharacter(character);
    }
}

