import { Game } from "../singleton/Game";
import { ICliCommand } from "./ICliCommand";
import { SelectCharacter } from "./SelectCharacter";

export class SelectList implements ICliCommand {
    getName(): string {
        return "Select a character";
    }
    execute(): ICliCommand[] {
        let characters = Game.getInstance().listCharacter();

        return characters.map(char => new SelectCharacter(char));
    }

}