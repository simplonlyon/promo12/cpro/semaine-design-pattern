
export interface ICliCommand {
    getName():string;
    execute():ICliCommand[];
}