import { ICliCommand } from "./ICliCommand";

export class Exit implements ICliCommand {
    getName(): string {
        return "Exit";
    }
    execute(): ICliCommand[] {
        process.exit();
    }

}