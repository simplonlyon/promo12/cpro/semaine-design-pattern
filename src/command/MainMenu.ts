import { Exit } from "./Exit";
import { ICliCommand } from "./ICliCommand";
import { SelectList } from "./SelectList";

export class MainMenu implements ICliCommand {
    getName(): string {
        return "Back to Main Menu";
    }
    execute(): ICliCommand[] {
        return [
            new SelectList(),
            new Exit()
        ]
    }

}