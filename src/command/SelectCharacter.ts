import { CharacterDto } from "../core/Character";
import { Game } from "../singleton/Game";
import { MainMenu } from "./MainMenu";
import { ICliCommand } from "./ICliCommand";

export class SelectCharacter implements ICliCommand {
    constructor(private character:CharacterDto){}
    getName(): string {
        return this.character.name;
    }
    execute(): ICliCommand[] {
        
        Game.getInstance().select(this.character.position);

        return [

            new MainMenu()
        ]
    }

    

}