import { Character } from "../core/Character";
import { IWeapon } from "./IWeapon";

export class SpellBook implements IWeapon {    
    protected character:Character;

    attack(target: Character): number {
        
        target.changeHp(-10);
        this.character.changeMp(-10);
        return 10;
    }
    getName(): string {
        return "SpellBook";
    }

    canAttack(target: Character): boolean {
        return true;
    }

    getCharacter() {
        return this.character;
    }
    setCharacter(character: Character): void {
        this.character = character;
    }
}