import { Character } from "../core/Character";
import { IWeapon } from "./IWeapon";

export class NoWeapon implements IWeapon {
    protected character:Character;
    attack(target: Character): number {
        console.log("Can't attack without weapon");
        return 0;
    }
    getName(): string {
        return "No Weapon";
    }

    canAttack(target: Character): boolean {
        return false;
    }

    getCharacter() {
        return this.character;
    }
    setCharacter(character: Character): void {
        this.character = character;
    }
}