import { Character } from "../core/Character";
import { IWeapon } from "./IWeapon";

export class Bow implements IWeapon {
    protected character:Character; 
    constructor(private damage:number = 7){}
    setCharacter(character: Character): void {
        this.character = character;
    }

    attack(target: Character): number {
        target.changeHp(-this.damage);
        this.character.changeStamina(-5);
        return this.damage;
    }
    getName(): string {
        return "Bow"
    }

    canAttack(target: Character): boolean {
        let posTarget = target.getPosition();
        let posSelf = this.character.getPosition();
        return posTarget.x === posSelf.x 
        || posTarget.y === posSelf.y;
    }

    getCharacter() {
        return this.character;
    }

}