import { Character } from "../core/Character";

export interface IWeapon {
    setCharacter(character:Character):void;
    canAttack(target:Character): boolean;
    attack(target:Character):number;
    getName():string;
    getCharacter():Character;
}