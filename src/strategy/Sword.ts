import { Character } from "../core/Character";
import { IWeapon } from "./IWeapon";

export class Sword implements IWeapon {
    protected character:Character;


    attack(target: Character): number {
        target.changeHp(-10);
        this.character.changeStamina(-5);
        return 10;
    }
    
    getName(): string {
        return "Sword";
    }

    canAttack(target: Character): boolean {
        let posTarget = target.getPosition();
        let posSelf = this.character.getPosition();
        //Avec l'épée, le personnage peut attaquer les cibles au dessus, en dessous et à côté d'elle
        return ( 
                  (posSelf.x === posTarget.x - 1 && posSelf.y === posTarget.y)  ||
                  (posSelf.x === posTarget.x + 1 && posSelf.y === posTarget.y)  ||        
                  (posSelf.y === posTarget.y - 1 && posSelf.x === posTarget.x)  ||  
                  (posSelf.y === posTarget.y + 1 && posSelf.x === posTarget.x) 
        );
    }

    getCharacter() {
        return this.character;
    }
    setCharacter(character: Character): void {
        this.character = character;
    }
}